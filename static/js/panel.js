/*
 * panel.js
 * Copyright (C) 2019 nicolasfara <nicolas.farabegoli@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

$.fn.api.settings.api.predict = '/api/v1/predict/{model}';

$(document).ready(() => {
  $('.ui.radio.checkbox').checkbox();
  $('select.dropdown').dropdown();
  $('#form_button').click(() => {
    $('form').submit()
  });

  $('#reset_button').click(() => {
    $('form').trigger("reset");
    $('#disease-result').text('');
    $('#disease-confidence').text('')
  })
});

$('.ui.form').form({
  fields: {
    gender: 'empty',
    age: 'empty',
    height: 'empty',
    weight: 'empty',
    ap_lo: 'empty',
    ap_hi: 'empty',
    cholesterol: 'empty',
    glucose: 'empty',
    smoking: 'empty',
    physical: 'empty',
    alchool: 'empty',
    model: 'empty',
  }
}).api({
  action: 'predict',
  method       : 'GET',
  serializeForm: true,
  beforeSend   : function(settings) {
    settings.urlData = {
      model: $('#model_type').val()
    };
    return settings
  },
  onSuccess    : function(response) {
    let pred = $('#disease-result');
    if (response.hasOwnProperty('error')) {
      pred.text(response.error);
      return
    }
    pred.text(response.result);
    $('#disease-prob').text(response.prob)
  },
  onFailure    : function(response) {
    console.log(response);
  }
});