from flask import Flask, render_template, jsonify
from flask import request
import pickle
import pandas as pd


app = Flask(__name__)


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/panel')
def panel():
    return render_template('panel.html')


@app.route('/api/v1/predict/<model>', methods=['GET'])
def predict(model):

    if model == 'xgb':
        with open('static/models/xgb-model.mdl', 'rb') as xgb_file:
            ml_model = pickle.load(xgb_file)
    elif model == 'svm':
        with open('static/models/svm-model.mdl', 'rb') as svm_file:
            ml_model = pickle.load(svm_file)
    elif model == 'lr':
        with open('static/models/lr-model.mdl', 'rb') as lr_file:
            ml_model = pickle.load(lr_file)
    elif model == 'pct':
        with open('static/models/pct-model.mdl', 'rb') as pct_file:
            ml_model = pickle.load(pct_file)
    elif model == 'rfc':
        with open('static/models/rf-model.mdl', 'rb') as rfc_file:
            ml_model = pickle.load(rfc_file)
    else:
        return jsonify({'error': 'Unable to predict with selected model'})

    features = ['age', 'gender', 'height', 'weight', 'ap_hi', 'ap_lo', 'cholesterol', 'gluc', 'smoke', 'alco', 'active', 'bmi']

    age = request.args.get('age')
    gender = request.args.get('gender')
    height = request.args.get('height')
    weight = request.args.get('weight')
    ap_hi = request.args.get('ap_hi')
    ap_lo = request.args.get('ap_lo')
    cholesterol = request.args.get('cholesterol')
    gluc = request.args.get('glucose')
    smoke = request.args.get('smoking')
    alco = request.args.get('alchool')
    active = request.args.get('physical')
    bmi = int(weight) * (int(height) / 100)**2

    data = [[age, gender, height, weight, ap_hi, ap_lo, cholesterol, gluc, smoke, alco, active, bmi]]

    dataframe = pd.DataFrame(data, columns=features)
    print(dataframe)

    cholesterol_enum = [1, 2, 3]
    glucose_enum = [1, 2, 3]

    cholesterol_dummy = pd.get_dummies(dataframe['cholesterol'], prefix='', prefix_sep='')
    cholesterol_dummy = cholesterol_dummy.T.reindex(cholesterol_enum).T.fillna(0)
    glucose_dummy = pd.get_dummies(dataframe['gluc'], prefix='', prefix_sep='')
    glucose_dummy = glucose_dummy.T.reindex(glucose_enum).T.fillna(0)

    dataframe = pd.concat([dataframe, cholesterol_dummy, glucose_dummy], axis=1)
    dataframe.drop(['cholesterol', 'gluc'], axis=1, inplace=True)
    print(dataframe.shape)

    prediction = ml_model.predict(dataframe)
    result = 'No disease' if prediction[0] == 0 else 'Possible disease'
    if model == 'svm':
        prob = 'Not available'
    elif model == 'rfc':
        prob = 'Not available'
    else:
        prob = ml_model.predict_proba(dataframe)
        prob = round(prob[0, prediction[0]] * 100, 3)
        prob = str(prob) + '%'

    return jsonify({'result': result, 'prob': prob})


if __name__ == '__main__':
    app.run()
